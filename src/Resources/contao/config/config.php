<?php

/**
 * 361GRAD Element Productoverview
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_productoverview_start'] =
    'Dse\\ElementsBundle\\ElementProductoverview\\Element\\ContentDseProductoverviewStart';
$GLOBALS['TL_CTE']['dse_elements']['dse_productoverview'] =
    'Dse\\ElementsBundle\\ElementProductoverview\\Element\\ContentDseProductoverview';
$GLOBALS['TL_CTE']['dse_elements']['dse_productoverview_stop']  =
    'Dse\\ElementsBundle\\ElementProductoverview\\Element\\ContentDseProductoverviewStop';

$GLOBALS['TL_WRAPPERS']['start'][] = 'dse_productoverview_start';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'dse_productoverview_stop';