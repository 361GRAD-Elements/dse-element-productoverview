<?php

/**
 * 361GRAD Element Productoverview
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_productoverview_start'] =
    '{type_legend},type,headline,' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_productoverview'] =
    '{type_legend},type;' .
    '{firsttile_legend},dse_firstTileTitle,dse_firstTileImage,dse_firstTileImageSize,dse_firstTileImageAlt,dse_firstTileLink,dse_firstTileTarget;' .
    '{firstfilterwidth_legend},dse_firstTileWidth1,dse_firstTileWidth2,dse_firstTileWidth3,dse_firstTileWidth4,dse_firstTileWidth5,dse_firstTileWidth6;' .
    '{firstfilterwoodtype_legend},dse_firstTileWoodType1,dse_firstTileWoodType2;' .
    '{firstfilterlength_legend},dse_firstTileLong1,dse_firstTileLong2;' .
    '{firstfiltersurface_legend},dse_firstTileSurface1,dse_firstTileSurface2,dse_firstTileSurface3;' .
    '{firstfiltersorting_legend},dse_firstTileSorting1,dse_firstTileSorting2;' .
    '{firstfiltertype_legend},dse_firstTileType1,dse_firstTileType2,dse_firstTileType3,dse_firstTileType4,dse_firstTileType5,dse_firstTileType6;' .
    
    '{secondtile_legend},dse_secondTileTitle,dse_secondTileImage,dse_secondTileImageSize,dse_secondTileImageAlt,dse_secondTileLink,dse_secondTileTarget;' .
    '{secondfilterwidth_legend},dse_secondTileWidth1,dse_secondTileWidth2,dse_secondTileWidth3,dse_secondTileWidth4,dse_secondTileWidth5,dse_secondTileWidth6;' .
    '{secondfilterwoodtype_legend},dse_secondTileWoodType1,dse_secondTileWoodType2;' .
    '{secondfilterlength_legend},dse_secondTileLong1,dse_secondTileLong2;' .
    '{secondfiltersurface_legend},dse_secondTileSurface1,dse_secondTileSurface2,dse_secondTileSurface3;' .
    '{secondfiltersorting_legend},dse_secondTileSorting1,dse_secondTileSorting2;' .
    '{secondfiltertype_legend},dse_secondTileType1,dse_secondTileType2,dse_secondTileType3,dse_secondTileType4,dse_secondTileType5,dse_secondTileType6;' .
    
    '{thirdtile_legend},dse_thirdTileTitle,dse_thirdTileImage,dse_thirdTileImageSize,dse_thirdTileImageAlt,dse_thirdTileLink,dse_thirdTileTarget;' .
    '{thirdfilterwidth_legend},dse_thirdTileWidth1,dse_thirdTileWidth2,dse_thirdTileWidth3,dse_thirdTileWidth4,dse_thirdTileWidth5,dse_thirdTileWidth6;' .
    '{thirdfilterwoodtype_legend},dse_thirdTileWoodType1,dse_thirdTileWoodType2;' .
    '{thirdfilterlength_legend},dse_thirdTileLong1,dse_thirdTileLong2;' .
    '{thirdfiltersurface_legend},dse_thirdTileSurface1,dse_thirdTileSurface2,dse_thirdTileSurface3;' .
    '{thirdfiltersorting_legend},dse_thirdTileSorting1,dse_thirdTileSorting2;' .
    '{thirdfiltertype_legend},dse_thirdTileType1,dse_thirdTileType2,dse_thirdTileType3,dse_thirdTileType4,dse_thirdTileType5,dse_thirdTileType6;' .
    
    '{fourthtile_legend},dse_fourthTileTitle,dse_fourthTileImage,dse_fourthTileImageSize,dse_fourthTileImageAlt,dse_fourthTileLink,dse_fourthTileTarget;' .
    '{fourthfilterwidth_legend},dse_fourthTileWidth1,dse_fourthTileWidth2,dse_fourthTileWidth3,dse_fourthTileWidth4,dse_fourthTileWidth5,dse_fourthTileWidth6;' .
    '{fourthfilterwoodtype_legend},dse_fourthTileWoodType1,dse_fourthTileWoodType2;' .
    '{fourthfilterlength_legend},dse_fourthTileLong1,dse_fourthTileLong2;' .
    '{fourthfiltersurface_legend},dse_fourthTileSurface1,dse_fourthTileSurface2,dse_fourthTileSurface3;' .
    '{fourthfiltersorting_legend},dse_fourthTileSorting1,dse_fourthTileSorting2;' .
    '{fourthfiltertype_legend},dse_fourthTileType1,dse_fourthTileType2,dse_fourthTileType3,dse_fourthTileType4,dse_fourthTileType5,dse_fourthTileType6;' .
    
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_productoverview_stop']  =
    '{type_legend},type;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests;' .
    '{invisible_legend:hide},invisible,start,stop';


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWidth6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWoodType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWoodType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileWoodType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWoodType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileLong1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLong1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileLong2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLong2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileSurface1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileSurface2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileSurface3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileSorting1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSorting1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileSorting2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSorting2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileType6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWidth6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWoodType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWoodType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileWoodType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWoodType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileLong1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLong1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileLong2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLong2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileSurface1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileSurface2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileSurface3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileSorting1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSorting1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileSorting2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSorting2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileType6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWidth6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWoodType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWoodType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileWoodType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWoodType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileLong1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLong1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileLong2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLong2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileSurface1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileSurface2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileSurface3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileSorting1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSorting1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileSorting2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSorting2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileType6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWidth6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWoodType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWoodType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileWoodType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWoodType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileLong1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLong1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileLong2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLong2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileSurface1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileSurface2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileSurface3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileSorting1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSorting1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileSorting2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSorting2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType1'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType1'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType2'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType2'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType3'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType3'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType4'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType4'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType5'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType5'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_fourthTileType6'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType6'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w20',
    ],
    'sql'       => "mediumblob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];