<?php

/**
 * 361GRAD Element Productoverview
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']              = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_productoverview_start'] = ['Produktübersicht Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_productoverview']       = ['Produktübersicht', 'Produktübersicht Teaser.'];
$GLOBALS['TL_LANG']['CTE']['dse_productoverview_stop']  = ['Produktübersicht Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['firsttile_legend']            = 'Erste Kachel';
$GLOBALS['TL_LANG']['tl_content']['firstfilterwidth_legend']     = 'Option - Breite';
$GLOBALS['TL_LANG']['tl_content']['firstfilterwoodtype_legend']  = 'Option - Holzart';
$GLOBALS['TL_LANG']['tl_content']['firstfilterlength_legend']    = 'Option - Länge';
$GLOBALS['TL_LANG']['tl_content']['firstfiltersurface_legend']   = 'Option - Oberfläche';
$GLOBALS['TL_LANG']['tl_content']['firstfiltersorting_legend']   = 'Option - Sortierung';
$GLOBALS['TL_LANG']['tl_content']['firstfiltertype_legend']      = 'Option - Typ';
$GLOBALS['TL_LANG']['tl_content']['secondtile_legend']           = 'Zweite Kachel';
$GLOBALS['TL_LANG']['tl_content']['secondfilterwidth_legend']    = 'Option - Breite';
$GLOBALS['TL_LANG']['tl_content']['secondfilterwoodtype_legend'] = 'Option - Holzart';
$GLOBALS['TL_LANG']['tl_content']['secondfilterlength_legend']   = 'Option - Länge';
$GLOBALS['TL_LANG']['tl_content']['secondfiltersurface_legend']  = 'Option - Oberfläche';
$GLOBALS['TL_LANG']['tl_content']['secondfiltersorting_legend']  = 'Option - Sortierung';
$GLOBALS['TL_LANG']['tl_content']['secondfiltertype_legend']     = 'Option - Typ';
$GLOBALS['TL_LANG']['tl_content']['thirdtile_legend']            = 'Dritte Kachel';
$GLOBALS['TL_LANG']['tl_content']['thirdfilterwidth_legend']     = 'Option - Breite';
$GLOBALS['TL_LANG']['tl_content']['thirdfilterwoodtype_legend']  = 'Option - Holzart';
$GLOBALS['TL_LANG']['tl_content']['thirdfilterlength_legend']    = 'Option - Länge';
$GLOBALS['TL_LANG']['tl_content']['thirdfiltersurface_legend']   = 'Option - Oberfläche';
$GLOBALS['TL_LANG']['tl_content']['thirdfiltersorting_legend']   = 'Option - Sortierung';
$GLOBALS['TL_LANG']['tl_content']['thirdfiltertype_legend']      = 'Option - Typ';
$GLOBALS['TL_LANG']['tl_content']['fourthtile_legend']           = 'Vierte Kachel';
$GLOBALS['TL_LANG']['tl_content']['fourthfilterwidth_legend']    = 'Option - Breite';
$GLOBALS['TL_LANG']['tl_content']['fourthfilterwoodtype_legend'] = 'Option - Holzart';
$GLOBALS['TL_LANG']['tl_content']['fourthfilterlength_legend']   = 'Option - Länge';
$GLOBALS['TL_LANG']['tl_content']['fourthfiltersurface_legend']  = 'Option - Oberfläche';
$GLOBALS['TL_LANG']['tl_content']['fourthfiltersorting_legend']  = 'Option - Sortierung';
$GLOBALS['TL_LANG']['tl_content']['fourthfiltertype_legend']     = 'Option - Typ';

$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth1']    = ['100er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth2']    = ['200er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth3']    = ['300er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth4']    = ['400er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth5']    = ['500er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWidth6']    = ['Mixed Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWoodType1'] = ['Douglasie', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileWoodType2'] = ['Eiche', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLong1']     = ['Fallend', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLong2']     = ['Raumlang', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface1']  = ['Geseift', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface2']  = ['Geölt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSurface3']  = ['Lackiert', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSorting1']  = ['Select', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileSorting2']  = ['Natur', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType1']     = ['Residential', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType2']     = ['Gallery', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType3']     = ['Office', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType4']     = ['Medical', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType5']     = ['Hotel/Gastro', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileType6']     = ['Showroom', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth1']    = ['100er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth2']    = ['200er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth3']    = ['300er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth4']    = ['400er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth5']    = ['500er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWidth6']    = ['Mixed Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWoodType1'] = ['Douglasie', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileWoodType2'] = ['Eiche', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLong1']     = ['Fallend', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLong2']     = ['Raumlang', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface1']  = ['Geseift', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface2']  = ['Geölt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSurface3']  = ['Lackiert', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSorting1']  = ['Select', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileSorting2']  = ['Natur', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType1']     = ['Residential', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType2']     = ['Gallery', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType3']     = ['Office', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType4']     = ['Medical', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType5']     = ['Hotel/Gastro', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileType6']     = ['Showroom', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth1']    = ['100er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth2']    = ['200er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth3']    = ['300er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth4']    = ['400er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth5']    = ['500er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWidth6']    = ['Mixed Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWoodType1'] = ['Douglasie', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileWoodType2'] = ['Eiche', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLong1']     = ['Fallend', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLong2']     = ['Raumlang', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface1']  = ['Geseift', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface2']  = ['Geölt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSurface3']  = ['Lackiert', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSorting1']  = ['Select', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileSorting2']  = ['Natur', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType1']     = ['Residential', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType2']     = ['Gallery', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType3']     = ['Office', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType4']     = ['Medical', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType5']     = ['Hotel/Gastro', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileType6']     = ['Showroom', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth1']    = ['100er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth2']    = ['200er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth3']    = ['300er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth4']    = ['400er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth5']    = ['500er Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWidth6']    = ['Mixed Series', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWoodType1'] = ['Douglasie', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileWoodType2'] = ['Eiche', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLong1']     = ['Fallend', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileLong2']     = ['Raumlang', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface1']  = ['Geseift', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface2']  = ['Geölt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSurface3']  = ['Lackiert', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSorting1']  = ['Select', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileSorting2']  = ['Natur', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType1']     = ['Residential', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType2']     = ['Gallery', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType3']     = ['Office', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType4']     = ['Medical', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType5']     = ['Hotel/Gastro', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_fourthTileType6']     = ['Showroom', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];